//
//  RecipeTableViewController.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "RecipeTableViewController.h"
#import "NarrowRecipeTableViewCell.h"
#import "UIImageView+R4SImageLoader.h"
#import "R4SNetworkClient.h"
#import <CoreData/CoreData.h>
#import "RecipeEngine.h"
#import "Recipe.h"

#import "AppDelegate.h"

typedef enum : NSUInteger {
    RecipeTableViewTypeNarrow,
    RecipeTableViewTypeWide,
} RecipeTableViewType;

@interface RecipeTableViewController ()<NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) RecipeTableViewType recipeTableViewType;

@end

@implementation RecipeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(loadRecipes)
                  forControlEvents:UIControlEventValueChanged];

    self.navigationItem.title = @"Рецепты";
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    }
    
    self.recipeTableViewType = RecipeTableViewTypeNarrow;

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
    [self loadRecipes];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Recipe" inManagedObjectContext:SharedManagedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"uid" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:SharedManagedObjectContext sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
    
}

- (void)setRecipeTableViewType:(RecipeTableViewType)recipeTableViewType
{
    _recipeTableViewType = recipeTableViewType;
    UIImage *image = nil;
    if (_recipeTableViewType == RecipeTableViewTypeWide)
    {
        _recipeTableViewType = RecipeTableViewTypeWide;
        image = [UIImage imageNamed:@"NarrowMenu"];
    } else {
        _recipeTableViewType = RecipeTableViewTypeNarrow;
        image = [UIImage imageNamed:@"WideMenu"];
    }
    
    UIButton *settingsView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [settingsView addTarget:self action:@selector(changeStyleTableView) forControlEvents:UIControlEventTouchUpInside];
    [settingsView setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:settingsView];
    [self.navigationItem setRightBarButtonItem:settingsButton];
    
    [self.tableView reloadData];
}

- (void)loadRecipes
{
    RecipeEngine *recipeEngine = [[RecipeEngine alloc] init];
    [recipeEngine loadRecipesWithSuccess:^(id response){
        [self.refreshControl endRefreshing];
    } fail:^(NSError *error){
        [self.refreshControl endRefreshing];
    }];
}

- (void)changeStyleTableView
{
    if (self.recipeTableViewType == RecipeTableViewTypeNarrow)
    {
        self.recipeTableViewType = RecipeTableViewTypeWide;
    } else {
        self.recipeTableViewType = RecipeTableViewTypeNarrow;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.recipeTableViewType) {
        case RecipeTableViewTypeNarrow:
            return 60;
            break;
        case RecipeTableViewTypeWide:
            return 184;
            break;
        default:
            break;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.recipeTableViewType == RecipeTableViewTypeNarrow)
    {
        NarrowRecipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"narrowCell" forIndexPath:indexPath];
        cell.recipe = [[self.fetchedResultsController fetchedObjects] objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    } else {
        NarrowRecipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"wideCell" forIndexPath:indexPath];
        cell.recipe = [[self.fetchedResultsController fetchedObjects] objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }

}

#pragma mark - NSFetchControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:{
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            if ([cell respondsToSelector:@selector(setRecipe:)])
            {
                Recipe *recipe = [[controller fetchedObjects] objectAtIndex:indexPath.row];
                [cell performSelector:@selector(setRecipe:) withObject:recipe];
            }
            break;
        }
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}
@end
