//
//  RecipeEngine.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "RecipeEngine.h"
#import "AppDelegate.h"

@implementation RecipeEngine

- (void)loadRecipesWithSuccess:(SucessBlock)success fail:(FailureBlock)fail;
{
    R4SNetworkClient* client = [[R4SNetworkClient alloc] init];
    [client loadRecipesWithSuccess:^(id response){
       
        NSError *error = nil;
        NSArray *jsonArray = [self serializationResponse:response error:error];
        if (error){
             [self failure:error fail:fail];
        } else {
            [self saveResponseToCoredata:jsonArray];
            if (success){
                success(nil);
            }
        }
        
    } fail:^(NSError *error){
        [self failure:error fail:fail];
    }];
}

- (void)failure:(NSError *)error fail:(FailureBlock)fail
{
     NSLog(@"%s %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    if (fail)
    {
        fail(error);
    }
}

- (NSArray *)serializationResponse:(id)response error:(NSError *)error
{
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:response
                                                         options: NSJSONReadingMutableContainers
                                                           error: &error];
    return jsonArray;
}

- (void)saveResponseToCoredata:(NSArray *)array
{
    for (NSDictionary *dict in array)
    {
        NSNumber *uid = [dict objectForKey:@"id"];
        if (![Recipe isExistingRecipeWithUID:uid])
        {
            Recipe *recipe = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Recipe class]) inManagedObjectContext:SharedManagedObjectContext];
            [recipe initWithDictionary:dict];
        }
    }
    [SharedManagedObjectContext save:nil];
}


@end
