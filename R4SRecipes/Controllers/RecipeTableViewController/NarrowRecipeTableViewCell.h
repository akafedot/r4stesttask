//
//  NarrowRecipeTableViewCell.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface NarrowRecipeTableViewCell : UITableViewCell

@property (nonatomic, strong) Recipe *recipe;

@end
