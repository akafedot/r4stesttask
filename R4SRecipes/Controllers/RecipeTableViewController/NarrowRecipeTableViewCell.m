//
//  NarrowRecipeTableViewCell.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "NarrowRecipeTableViewCell.h"
#import "UIImageView+R4SImageLoader.h"
#import "UIImage+Additions.h"

@interface NarrowRecipeTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeMinutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *recipeCaloriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;
@property (weak, nonatomic) IBOutlet UIView *recipeBackroundView;

@end

@implementation NarrowRecipeTableViewCell

- (void)setRecipe:(Recipe *)recipe
{
    _recipe = recipe;
    self.recipeNameLabel.text = recipe.name;
    self.recipeMinutesLabel.text = [NSString stringWithFormat:@"%@ мин.",recipe.minutes];
    self.recipeCaloriesLabel.text = [NSString stringWithFormat:@"%@ ККАЛ",recipe.calories];
    
    NSURL *imageURL = [recipe recipeImageURLWithSize:self.recipeImageView.frame.size];
    [self.recipeImageView setImageWithURL:imageURL placeholder:@"placeholder" completion:^(UIImage *img){
        self.recipeBackroundView.backgroundColor = [img avgColor];
    }];
}

@end
