//
//  RecipeEngine.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "R4SNetworkClient.h"
#import "Recipe.h"

@interface RecipeEngine : NSObject

- (void)loadRecipesWithSuccess:(SucessBlock)success fail:(FailureBlock)fail;

@end
