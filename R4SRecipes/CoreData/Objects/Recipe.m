//
//  Recipe.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "Recipe.h"
#import "AppDelegate.h"

@implementation Recipe

- (void)initWithDictionary:(NSDictionary *)dictionary
{
    self.uid      = [dictionary objectForKey:@"id"];
    self.calories = [dictionary objectForKey:@"calories"];
    self.minutes  = [dictionary objectForKey:@"minutes"];
    self.imageUID = [dictionary objectForKey:@"image"];
    self.name     = [dictionary objectForKey:@"name"];
}

- (NSURL *)recipeImageURLWithSize:(CGSize)size
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://image-server.readyforsky.com/i/%@/%dx%d.png",self.imageUID,(int)size.width, (int)size.height]];
}

+ (BOOL)isExistingRecipeWithUID:(NSNumber *)uid
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
    fetchRequest.resultType = NSCountResultType;
    NSUInteger count = [SharedManagedObjectContext countForFetchRequest:fetchRequest error:NULL];
    return count ? YES : NO;
}

@end
