//
//  Recipe+CoreDataProperties.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Recipe.h"

NS_ASSUME_NONNULL_BEGIN

@interface Recipe (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *imageUID;
@property (nullable, nonatomic, retain) NSNumber *minutes;
@property (nullable, nonatomic, retain) NSNumber *calories;

@end

NS_ASSUME_NONNULL_END
