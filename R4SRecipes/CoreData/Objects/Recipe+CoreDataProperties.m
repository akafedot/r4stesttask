//
//  Recipe+CoreDataProperties.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Recipe+CoreDataProperties.h"

@implementation Recipe (CoreDataProperties)

@dynamic uid;
@dynamic name;
@dynamic imageUID;
@dynamic minutes;
@dynamic calories;

@end
