//
//  Recipe.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Recipe : NSManagedObject

- (void)initWithDictionary:(NSDictionary *)dictionary;
- (NSURL *)recipeImageURLWithSize:(CGSize)size;

+ (BOOL)isExistingRecipeWithUID:(NSNumber *)uid;

@end

NS_ASSUME_NONNULL_END

#import "Recipe+CoreDataProperties.h"
