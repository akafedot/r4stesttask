//
//  UIImageView+R4SImageLoader.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "UIImageView+R4SImageLoader.h"
#import <objc/runtime.h>

@interface UIImageView (_R4SImageLoader)
@property (nonatomic, strong,setter = imageRequestBlockOperation:) NSBlockOperation *imageRequestBlockOperation;
@property (nonatomic, strong,setter = cache:)            NSCache          *cache;
@end

@implementation UIImageView (R4SImageLoader)

+ (NSOperationQueue *)sharedImageRequestOperationQueue {
    static NSOperationQueue *_sharedImageRequestOperationQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedImageRequestOperationQueue = [[NSOperationQueue alloc] init];
        _sharedImageRequestOperationQueue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount;
    });
    return _sharedImageRequestOperationQueue;
}

+ (NSCache *)sharedImageCache {
    static NSCache *sharedImageCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedImageCache = [[NSCache alloc] init];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
            [sharedImageCache removeAllObjects];
        }];
    });
    return objc_getAssociatedObject(self, @selector(sharedImageCache)) ?: sharedImageCache;
}

- (NSBlockOperation *)imageRequestBlockOperation {
    return (NSBlockOperation *)objc_getAssociatedObject(self, @selector(imageRequestBlockOperation));
}

- (void)imageRequestBlockOperation:(NSBlockOperation *)imageRequestOperation {
    objc_setAssociatedObject(self, @selector(imageRequestBlockOperation), imageRequestOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)cache:(NSCache *)cache
{
    objc_setAssociatedObject(self, @selector(cache), cache, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setImageWithURL:(NSURL *)url placeholder:(NSString *)placeholderName completion:(void(^)(UIImage *image))completion{

    [self cancelImageRequestOperation];
    
    UIImage *cachedImage = [[UIImageView sharedImageCache] objectForKey:[url absoluteString]];
    
    if (cachedImage) {
        if (completion) {
            completion(cachedImage);
        }
        self.image = cachedImage;
        self.imageRequestBlockOperation = nil;
    } else {
        UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
        if (placeholderImage) {
            self.image = placeholderImage;
        }
        
        __weak __typeof(self)weakSelf = self;
        
        self.imageRequestBlockOperation = [NSBlockOperation blockOperationWithBlock:^(){
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *img = [[UIImage alloc] initWithData:data];
            
            if (img) {
                [[UIImageView sharedImageCache] setObject:img forKey:[url absoluteString]];
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    weakSelf.image = img;
                    if (completion)
                    {
                        completion(img);
                    }
                });
            }
        }];
        
        [[[self class] sharedImageRequestOperationQueue] addOperation:self.imageRequestBlockOperation];
    }
}

- (void)cancelImageRequestOperation {
    [[self imageRequestBlockOperation] cancel];
    self.imageRequestBlockOperation = nil;
}

@end
