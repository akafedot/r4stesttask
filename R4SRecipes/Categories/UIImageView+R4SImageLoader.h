//
//  UIImageView+R4SImageLoader.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (R4SImageLoader)

- (void)setImageWithURL:(NSURL *)url
            placeholder:(NSString *)placeholderName
             completion:(void(^)(UIImage *image))completion;

@end
