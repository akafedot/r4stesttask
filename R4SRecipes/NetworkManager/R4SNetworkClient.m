//
//  R4SNetworkClient.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "R4SNetworkClient.h"

static NSString * const kRecipeLoadingRecipes =  @"http://ios.test.readyforsky.com/recipes.json";

@implementation R4SNetworkClient

- (void)loadRecipesWithSuccess:(SucessBlock)success fail:(FailureBlock)fail {
    [self requestURLString:kRecipeLoadingRecipes success:success fail:fail];
}

@end
