//
//  BaseNetworkClient.m
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "BaseNetworkClient.h"

@interface BaseNetworkClient()<NSURLConnectionDelegate>

@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, copy)   SucessBlock    successBlock;
@property (nonatomic, copy)   FailureBlock   failureBlock;

@end

@implementation BaseNetworkClient

- (void)requestURLString:(NSString *)URLString success:(SucessBlock)success fail:(FailureBlock)fail
{
    self.successBlock = success;
    self.failureBlock = fail;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!connection){
        NSLog(@"%s : NSURLConnnection is nil", __PRETTY_FUNCTION__);
    }
    
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (self.successBlock)
    {
        self.successBlock(self.responseData);
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (self.failureBlock)
    {
        self.failureBlock(error);
    }
}


@end
