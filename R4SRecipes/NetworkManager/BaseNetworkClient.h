//
//  BaseNetworkClient.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SucessBlock)(id response);
typedef void(^FailureBlock)(NSError *error);

@interface BaseNetworkClient : NSObject

- (void)requestURLString:(NSString *)URLString success:(SucessBlock)success fail:(FailureBlock)fail;

@end
