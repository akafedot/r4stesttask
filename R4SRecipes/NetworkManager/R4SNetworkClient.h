//
//  R4SNetworkClient.h
//  R4SRecipes
//
//  Created by Fedotov Andrew on 04.10.15.
//  Copyright © 2015 fedotov.andrew. All rights reserved.
//

#import "BaseNetworkClient.h"

@interface R4SNetworkClient : BaseNetworkClient

- (void)loadRecipesWithSuccess:(SucessBlock)success fail:(FailureBlock)fail;

@end
